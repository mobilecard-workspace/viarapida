/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.constants;

public class ViaRapidaConstants {
	
	public static final int TIPO_RECARGA = 6;
	public static final String DATE_FORMAT = "ddMMyyHHmmSSzzz";
	public static final int SUCCESS_CODE = 1;
	
}
