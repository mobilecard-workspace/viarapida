/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.constants;

public class StatusConstants {

	//Codigos de error
	public static final int SUCCESS_CODE = 0;
	public static final int VIARAPIDA_CONNECTION_CODE = -1;
	public static final int VIARAPIDA_SERVICE_CODE = -2;
	public static final int INTERNAL_ERROR_CODE = -3;
	public static final int USER_NOT_FOUND_CODE = -4;
	
	//Mensajes de error
	public static final String SUCCESS_MESSAGE = "Se ha procesado correctamente la peticion";
	public static final String VIARAPIDA_CONNECTION_MESSAGE = "No se pudo conectar con el WS de Via Rapida";
	public static final String VIARAPIDA_SERVICE_MESSAGE = "El WS de Via Rapida regreso un error";
	public static final String INTERNAL_ERROR_MESSAGE = "Se ha enviado correctamente el dinero";
	public static final String USER_NOT_FOUND_MESSAGE = "Usuario no encontrado o bloqueado";
	
}
