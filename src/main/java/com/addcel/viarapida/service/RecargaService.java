/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.addcel.viarapida.constants.MobileCardConstants;
import com.addcel.viarapida.constants.StatusConstants;
import com.addcel.viarapida.constants.ViaRapidaConstants;
import com.addcel.viarapida.entity.TUsuarios;
import com.addcel.viarapida.repository.TUsuariosRepository;
import com.addcel.viarapida.request.RecargarTagRequest;
import com.addcel.viarapida.response.ApiResponse;
import com.addcel.viarapida.util.PropertiesFile;
import com.addcel.viarapida.util.RestClient;
import com.addcel.viarapida.ws.request.RechargeReq;
import com.addcel.viarapida.ws.request.RechargeRequest;
import com.addcel.viarapida.ws.response.RechargeResponse;

@Service
public class RecargaService {
	
	private static final Logger LOGGER = LogManager.getLogger(RecargaService.class);
	
	@Autowired
	private RestClient restClient;
	
	@Autowired
	private PropertiesFile propsFile;
	
	@Autowired
	private TUsuariosRepository tUsuariosRepo;
	
	public ApiResponse recargarTag(RecargarTagRequest recargarTagReq, int idApp, int idPais, String idioma) {
		ApiResponse apiResp = new ApiResponse();
		RechargeResponse response = null;
		SimpleDateFormat sdf = new SimpleDateFormat(ViaRapidaConstants.DATE_FORMAT);
		
		LOGGER.info("Buscando usuario con ID: " + recargarTagReq.getIdUsuario());
		TUsuarios usuario = tUsuariosRepo.findOne(recargarTagReq.getIdUsuario());
		
		if(usuario != null && usuario.getIdUsrStatus() != MobileCardConstants.USUARIO_ACTIVO) {
			LOGGER.warn("No se encontro al usuario o se encuentra bloqueado");
			apiResp.setCode(StatusConstants.USER_NOT_FOUND_CODE);
			apiResp.setMessage(StatusConstants.USER_NOT_FOUND_MESSAGE);
			
			return apiResp;
		}
		
		LOGGER.debug("Usuario encontrado correctamente: " + usuario.toString());
		
		RechargeReq recharge = new RechargeReq();
		recharge.setAuthorizationNumber(recargarTagReq.getAuthNumber());
		recharge.setDate(sdf.format(new Date()));
		recharge.setTagId("");
		recharge.setType(ViaRapidaConstants.TIPO_RECARGA);
		recharge.setUserId(usuario.getIdUsuario());
		recharge.setValue(recargarTagReq.getMonto());
		recharge.setVehiclePlate(recargarTagReq.getPlaca());
		
		RechargeRequest rechargeReq = new RechargeRequest();
		rechargeReq.setCodePlatform(propsFile.getViarapidaCodePlatform());
		rechargeReq.setRecharge(recharge);
		
		try {
			LOGGER.info("Consultando la informacion del TAG con Via Rapida ...");
			response = (RechargeResponse) restClient.createRequest(propsFile.getViarapidaUrlRecarga(), HttpMethod.POST, rechargeReq, RechargeResponse.class);
		} catch (Exception ex) {
			LOGGER.error("Ocurrio un error al consumir el servicio de Via Rapida: " + ex.getMessage());
			ex.printStackTrace();
			
			apiResp.setCode(StatusConstants.VIARAPIDA_CONNECTION_CODE);
			apiResp.setMessage(StatusConstants.VIARAPIDA_CONNECTION_MESSAGE);
			return apiResp;
		}
		
		if(response.getResponse().getId() == ViaRapidaConstants.SUCCESS_CODE) {
			LOGGER.info("Se consulto correctamente la informacion del TAG: " + response.getResponse().getDesc());
			LOGGER.debug("Informacion de la recarga del TAG: " + response.getRecharge().toString());
			
			apiResp.setCode(StatusConstants.SUCCESS_CODE);
			apiResp.setMessage(StatusConstants.SUCCESS_MESSAGE);
			apiResp.setData(response.getRecharge());
		} else {
			LOGGER.warn("No se pudo consultar el TAG con Via Rapida: " + response.getResponse().getDesc());
			
			apiResp.setCode(StatusConstants.VIARAPIDA_SERVICE_CODE);
			apiResp.setMessage(StatusConstants.VIARAPIDA_SERVICE_MESSAGE);
		}
		
		return apiResp;
	}
	
}
