/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.addcel.viarapida.constants.MobileCardConstants;
import com.addcel.viarapida.constants.StatusConstants;
import com.addcel.viarapida.constants.ViaRapidaConstants;
import com.addcel.viarapida.entity.TUsuarios;
import com.addcel.viarapida.repository.TUsuariosRepository;
import com.addcel.viarapida.request.ConsultarSaldoRequest;
import com.addcel.viarapida.response.ApiResponse;
import com.addcel.viarapida.util.PropertiesFile;
import com.addcel.viarapida.util.RestClient;
import com.addcel.viarapida.ws.request.GetTagRequest;
import com.addcel.viarapida.ws.request.TagReq;
import com.addcel.viarapida.ws.response.TagResponse;

@Service
public class DispositivosService {
	
	private static final Logger LOGGER = LogManager.getLogger(DispositivosService.class);
	
	@Autowired
	private RestClient restClient;
	
	@Autowired
	private PropertiesFile propsFile;
	
	@Autowired
	private TUsuariosRepository tUsuariosRepo;
	
	public ApiResponse consultaTag(ConsultarSaldoRequest consultarTagReq, int idApp, int idPais, String idioma) {
		ApiResponse apiResp = new ApiResponse();
		TagResponse response = null;
		
		LOGGER.info("Buscando usuario con ID: " + consultarTagReq.getIdUsuario());
		TUsuarios usuario = tUsuariosRepo.findOne(consultarTagReq.getIdUsuario());
		
		if(usuario != null && usuario.getIdUsrStatus() != MobileCardConstants.USUARIO_ACTIVO) {
			LOGGER.warn("No se encontro al usuario o se encuentra bloqueado");
			apiResp.setCode(StatusConstants.USER_NOT_FOUND_CODE);
			apiResp.setMessage(StatusConstants.USER_NOT_FOUND_MESSAGE);
			
			return apiResp;
		}
		
		LOGGER.debug("Usuario encontrado correctamente: " + usuario.toString());
		
		TagReq tag = new TagReq();
		tag.setId(consultarTagReq.getTag());
		
		GetTagRequest getTagReq = new GetTagRequest();
		getTagReq.setCodePlatform(propsFile.getViarapidaCodePlatform());
		getTagReq.setTag(tag);
		
		try {
			LOGGER.info("Consultando la informacion del TAG con Via Rapida ...");
			response = (TagResponse) restClient.createRequest(propsFile.getViarapidaUrlSaldo(), HttpMethod.POST, getTagReq, TagResponse.class);
		} catch (Exception ex) {
			LOGGER.error("Ocurrio un error al consumir el servicio de Via Rapida: " + ex.getMessage());
			ex.printStackTrace();
			
			apiResp.setCode(StatusConstants.VIARAPIDA_CONNECTION_CODE);
			apiResp.setMessage(StatusConstants.VIARAPIDA_CONNECTION_MESSAGE);
			return apiResp;
		}
		
		if(response.getResponse().getId() == ViaRapidaConstants.SUCCESS_CODE) {
			LOGGER.info("Se consulto correctamente la informacion del TAG: " + response.getResponse().getDesc());
			LOGGER.debug("Informacion del TAG: " + response.getTag().toString());
			
			apiResp.setCode(StatusConstants.SUCCESS_CODE);
			apiResp.setMessage(StatusConstants.SUCCESS_MESSAGE);
			apiResp.setData(response.getTag());
		} else {
			LOGGER.warn("No se pudo consultar el TAG con Via Rapida: " + response.getResponse().getDesc());
			
			apiResp.setCode(StatusConstants.VIARAPIDA_SERVICE_CODE);
			apiResp.setMessage(StatusConstants.VIARAPIDA_SERVICE_MESSAGE);
		}
		
		return apiResp;
	}
	
}
