/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.service;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.addcel.viarapida.constants.MobileCardConstants;
import com.addcel.viarapida.constants.StatusConstants;
import com.addcel.viarapida.constants.ViaRapidaConstants;
import com.addcel.viarapida.entity.TUsuarios;
import com.addcel.viarapida.repository.TUsuariosRepository;
import com.addcel.viarapida.request.ConsultarPasosRequest;
import com.addcel.viarapida.response.ApiResponse;
import com.addcel.viarapida.util.PropertiesFile;
import com.addcel.viarapida.util.RestClient;
import com.addcel.viarapida.ws.request.PassReq;
import com.addcel.viarapida.ws.request.PassRequest;
import com.addcel.viarapida.ws.response.PassResponse;

@Service
public class PasosService {
	
	private static final Logger LOGGER = LogManager.getLogger(PasosService.class);
	
	@Autowired
	private RestClient restClient;
	
	@Autowired
	private PropertiesFile propsFile;
	
	@Autowired
	private TUsuariosRepository tUsuariosRepo;
	
	public ApiResponse consultaPasos(ConsultarPasosRequest consultarPasosReq, int idApp, int idPais, String idioma) {
		ApiResponse apiResp = new ApiResponse();
		PassResponse response = null;
		
		LOGGER.info("Buscando usuario con ID: " + consultarPasosReq.getIdUsuario());
		TUsuarios usuario = tUsuariosRepo.findOne(consultarPasosReq.getIdUsuario());
		
		if(usuario != null && usuario.getIdUsrStatus() != MobileCardConstants.USUARIO_ACTIVO) {
			LOGGER.warn("No se encontro al usuario o se encuentra bloqueado");
			apiResp.setCode(StatusConstants.USER_NOT_FOUND_CODE);
			apiResp.setMessage(StatusConstants.USER_NOT_FOUND_MESSAGE);
			
			return apiResp;
		}
		
		LOGGER.debug("Usuario encontrado correctamente: " + usuario.toString());
		
		PassReq pass = new PassReq();
		pass.setIdUsuario(consultarPasosReq.getIdUsuario());
		pass.setTagId(consultarPasosReq.getTag());
		pass.setTipoConsulta(propsFile.getViarapidaPasosTipoconsulta());
		
		PassRequest passReq = new PassRequest();
		passReq.setCodePlatform(propsFile.getViarapidaCodePlatform());
		passReq.setPass(pass);
		
		try {
			LOGGER.info("Consultando los pasos del TAG con Via Rapida ...");
			response = (PassResponse) restClient.createRequest(propsFile.getViarapidaUrlPasos(), HttpMethod.POST, passReq, PassResponse.class);
		} catch (Exception ex) {
			LOGGER.error("Ocurrio un error al consumir el servicio de Via Rapida: " + ex.getMessage());
			ex.printStackTrace();
			
			apiResp.setCode(StatusConstants.VIARAPIDA_CONNECTION_CODE);
			apiResp.setMessage(StatusConstants.VIARAPIDA_CONNECTION_MESSAGE);
			return apiResp;
		}
		
		if(response.getResponse().getId() == ViaRapidaConstants.SUCCESS_CODE) {
			LOGGER.info("Se consulto correctamente los pasos del TAG: " + response.getResponse().getDesc());
			LOGGER.debug("Pasos del TAG: " + Arrays.toString(response.getPass().toArray()));
			
			apiResp.setCode(StatusConstants.SUCCESS_CODE);
			apiResp.setMessage(StatusConstants.SUCCESS_MESSAGE);
			apiResp.setData(response.getPass());
		} else {
			LOGGER.warn("No se pudo consultar los pasos del TAG con Via Rapida: " + response.getResponse().getDesc());
			
			apiResp.setCode(StatusConstants.VIARAPIDA_SERVICE_CODE);
			apiResp.setMessage(StatusConstants.VIARAPIDA_SERVICE_MESSAGE);
		}
		
		return apiResp;
	}

}
