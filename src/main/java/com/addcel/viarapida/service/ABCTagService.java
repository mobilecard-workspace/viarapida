/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.viarapida.constants.StatusConstants;
import com.addcel.viarapida.repository.TUsuariosRepository;
import com.addcel.viarapida.request.AgregarTagRequest;
import com.addcel.viarapida.request.EliminarTagRequest;
import com.addcel.viarapida.response.ApiResponse;
import com.addcel.viarapida.util.PropertiesFile;
import com.addcel.viarapida.util.RestClient;

@Service
public class ABCTagService {

	private static final Logger LOGGER = LogManager.getLogger(ABCTagService.class);
	
	@Autowired
	private RestClient restClient;
	
	@Autowired
	private PropertiesFile propsFile;
	
	@Autowired
	private TUsuariosRepository tUsuariosRepo;
	
	public ApiResponse consultarTags(long idUsuario, int idApp, int idPais, String idioma) {
		//ApiResponse apiResp = new ApiResponse();
		
		
		return new ApiResponse(StatusConstants.SUCCESS_CODE, StatusConstants.SUCCESS_MESSAGE, null);
	}
	
	public ApiResponse agregarTag(AgregarTagRequest agregarTagReq, int idApp, int idPais, String idioma) {
		//ApiResponse apiResp = new ApiResponse();
		
		
		return new ApiResponse(StatusConstants.SUCCESS_CODE, StatusConstants.SUCCESS_MESSAGE, null);
	}
	
	public ApiResponse eliminarTag(EliminarTagRequest eliminarTagReq, int idApp, int idPais, String idioma) {
		//ApiResponse apiResp = new ApiResponse();
		
		
		return new ApiResponse(StatusConstants.SUCCESS_CODE, StatusConstants.SUCCESS_MESSAGE, null);
	}
	
}
