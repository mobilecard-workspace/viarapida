/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.ws.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PassResp {

	private String tagId;
	private String tagIdRefer;
	private String tagName;
	private Long userId;
	private String date;
	private Double value;
	private String desc;
	
}
