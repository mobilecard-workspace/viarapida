/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.ws.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TagResponse {

	private Response response;
	
	@JsonInclude(Include.NON_NULL)
	private TagRes tag;
	
}
