/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.ws.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RechargeRes {

	private int id;
	
}
