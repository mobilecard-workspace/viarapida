/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.ws.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PassResponse {

private Response response;
	
	@JsonInclude(Include.NON_NULL)
	private List<PassResp> pass;
	
}
