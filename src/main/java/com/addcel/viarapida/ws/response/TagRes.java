/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.ws.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TagRes {
	
	private long balance;
	private String id;
	private String name;
	private String plate;
	private int state;
	private String tagIdReferer;
	private long userId;
	
}
