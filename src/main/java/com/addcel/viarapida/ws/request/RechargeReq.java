/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.ws.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RechargeReq {
	
	private long value;
	private String date;
	private String tagId;
	private String vehiclePlate;
	private int type;
	private String authorizationNumber;
	private long userId;

}