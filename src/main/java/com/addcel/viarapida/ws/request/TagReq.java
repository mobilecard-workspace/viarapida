/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.ws.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TagReq {

	private String id;
	private String tagIdRefer;
	private String plate;
	
}
