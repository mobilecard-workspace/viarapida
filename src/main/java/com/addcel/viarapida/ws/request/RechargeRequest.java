/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.ws.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RechargeRequest {

	private RechargeReq recharge;
	private String codePlatform;
	
}
