/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.ws.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PassRequest {

	private PassReq pass;
	private String codePlatform;
}
