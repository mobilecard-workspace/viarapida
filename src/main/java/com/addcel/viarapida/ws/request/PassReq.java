/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.ws.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PassReq {
	
	private String tagId;
	private Long idUsuario;
	private Integer tipoConsulta;
	
}
