/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.request;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConsultarPasosRequest {

	@NotEmpty
	private String tag;
	
	@NotNull
	private Long idUsuario;
	
	@NotNull
	private Integer tipoConsulta;
	
}
