/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.request;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;

@Data
public class ConsultarSaldoRequest {
	
	@NotNull
	private Long idUsuario;
		
	private String tag;
	
	@NotEmpty
	private String placa;
		
}
