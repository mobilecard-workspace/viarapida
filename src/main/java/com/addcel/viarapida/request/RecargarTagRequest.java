/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.request;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;

@Data
public class RecargarTagRequest {
	
	@NotNull
	private Long idUsuario;

	@NotNull
	private Long monto;
	
	@NotEmpty
	private String authNumber;
		
	@NotEmpty
	private String placa;
	
}
