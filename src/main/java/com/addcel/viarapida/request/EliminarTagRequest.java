/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.request;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;

@Data
public class EliminarTagRequest {
	
	@NotNull
	private Long idUsuario;
	
	@NotEmpty
	private String tag;

}
