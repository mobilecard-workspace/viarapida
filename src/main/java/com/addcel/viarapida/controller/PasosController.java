/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.viarapida.constants.StatusConstants;
import com.addcel.viarapida.request.ConsultarPasosRequest;
import com.addcel.viarapida.response.ApiResponse;
import com.addcel.viarapida.service.PasosService;
import com.fasterxml.uuid.Generators;

@RestController
@RequestMapping("/api")
@Validated
public class PasosController {
	
	private static final Logger LOGGER = LogManager.getLogger(PasosController.class);
	
	@Autowired
	private PasosService pasosServ;
	
	@PostMapping(value = "/{idApp}/{idPais}/{idioma}/consultarPasos", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object consultarPasos(@Valid @RequestBody ConsultarPasosRequest consultarPasosReq, @PathVariable int idApp,
			@PathVariable int idPais, @PathVariable String idioma) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		ApiResponse resp = new ApiResponse();
		
		LOGGER.debug("ConsultarPasosRequest: " + consultarPasosReq.toString());
		LOGGER.info("Consultando los pasos del tag: " + consultarPasosReq.getTag());
		
		resp = pasosServ.consultaPasos(consultarPasosReq, idApp, idPais, idioma);
		
		if(resp.getCode() == StatusConstants.SUCCESS_CODE) {
			LOGGER.info("Se ha consultado correctamente los pasos del TAG");
		} else {
			LOGGER.warn("No se pudo consultar los pasos del TAG: " + resp.getMessage());
		}
		
		LOGGER.debug("Respuesta enviada al cliente: " + resp.toString());
		ThreadContext.clearAll();
		
		return resp;
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
		LOGGER.error("Ocurrio un mal consumo al servicio REST, no estan mandando bien los parametros");
		
		Map<String, String> errors = new HashMap<>();
		
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
	    });
	    
	    return errors;
	}
	
}
