/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.viarapida.constants.StatusConstants;
import com.addcel.viarapida.request.AgregarTagRequest;
import com.addcel.viarapida.request.EliminarTagRequest;
import com.addcel.viarapida.response.ApiResponse;
import com.addcel.viarapida.service.ABCTagService;
import com.fasterxml.uuid.Generators;

@RestController
@RequestMapping("/api")
@Validated
public class ABCTagController {

	private static final Logger LOGGER = LogManager.getLogger(ABCTagController.class);
	
	@Autowired
	private ABCTagService abcTagServ;
	
	@GetMapping(value = "/{idApp}/{idPais}/{idioma}/consultarTags/{idUsuario}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Object consultarTag(@PathVariable int idApp, @PathVariable long idUsuario, @PathVariable int idPais, @PathVariable String idioma) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		ApiResponse resp = new ApiResponse();
		
		LOGGER.info("Consultando los TAGs del usuario:" + idUsuario);
				
		resp = abcTagServ.consultarTags(idUsuario, idApp, idPais, idioma);
		
		if(resp.getCode() == StatusConstants.SUCCESS_CODE) {
			LOGGER.info("Se ha obtenido correctamente los TAGs de ese usuario");
		} else {
			LOGGER.warn("No se pudo obtener los TAGs de ese usuario");
		}
		
		LOGGER.debug("Respuesta enviada al cliente: " + resp.toString());
		ThreadContext.clearAll();
		
		return resp;
	}
	
	@PutMapping(value = "/{idApp}/{idPais}/{idioma}/agregarUsuario", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object agregarUsuario(@Valid @RequestBody AgregarTagRequest agregarTagReq, @PathVariable int idApp, 
			@PathVariable int idPais, @PathVariable String idioma) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		ApiResponse resp = new ApiResponse();
		
		LOGGER.info("Agregando TAG: [%s] al usuario: [%s]", agregarTagReq.getTag(), agregarTagReq.getIdUsuario());
				
		resp = abcTagServ.agregarTag(agregarTagReq, idApp, idPais, idioma);
		
		if(resp.getCode() == StatusConstants.SUCCESS_CODE) {
			LOGGER.info("Se ha agregado correctamente el TAG");
		} else {
			LOGGER.warn("No se pudo agregar el TAG");
		}
		
		LOGGER.debug("Respuesta enviada al cliente: " + resp.toString());
		ThreadContext.clearAll();
		
		return resp;
	}
	
	@PostMapping(value = "/{idApp}/{idPais}/{idioma}/eliminarTag", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object eliminarTag(@Valid @RequestBody EliminarTagRequest eliminarTagReq, @PathVariable int idApp, 
			@PathVariable int idPais, @PathVariable String idioma) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		ApiResponse resp = new ApiResponse();
		
		LOGGER.info("Eliminando el TAG:" + eliminarTagReq.getTag());
				
		resp = abcTagServ.eliminarTag(eliminarTagReq, idApp, idPais, idioma);
		
		if(resp.getCode() == StatusConstants.SUCCESS_CODE) {
			LOGGER.info("Se ha eliminado correctamente el TAG");
		} else {
			LOGGER.warn("No se pudo eliminar ese TAG");
		}
		
		LOGGER.debug("Respuesta enviada al cliente: " + resp.toString());
		ThreadContext.clearAll();
		
		return resp;
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
		LOGGER.error("Ocurrio un mal consumo al servicio REST, no estan mandando bien los parametros");
		
		Map<String, String> errors = new HashMap<>();
		
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
	    });
	    
	    return errors;
	}
	
}
