/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class ViaRapidaApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ViaRapidaApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(ViaRapidaApplication.class, args);
	}
	
}
