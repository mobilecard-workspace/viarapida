/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.util;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class RestClient {
	
	private static final Logger LOGGER = LogManager.getLogger(RestClient.class);
	
	private HttpHeaders addHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		return headers;
	}
	
	public Object createRequest(String uri, HttpMethod httpMethod, Object requestBody, Class<?> responseClass) 
			throws Exception {
		Object response = null;
		RestTemplate restTemplate = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		String body = mapper.writeValueAsString(requestBody);
		
		HttpEntity<String> entity = new HttpEntity<String>(body, addHeaders());
		
		LOGGER.debug("{} - {} ", httpMethod.toString(), uri);
		LOGGER.debug("Body Request: " + entity.getBody());
		
		ResponseEntity<?> result = restTemplate.exchange(uri, httpMethod, entity, responseClass);
		response = result.getBody();
		
		LOGGER.debug("Status Code: " + result.getStatusCode());
		LOGGER.debug("Body Response: " + result.getBody().toString());
		
		return response;
	}
	
}
