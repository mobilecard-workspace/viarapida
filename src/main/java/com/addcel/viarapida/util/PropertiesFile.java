/**
 * @author Victor Ramirez
 */

package com.addcel.viarapida.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@PropertySource("classpath:application.properties")
@Data
public class PropertiesFile {
		
	@Value("${viarapida.url.recarga}")
	private String viarapidaUrlRecarga;
	
	@Value("${viarapida.url.saldo}")
	private String viarapidaUrlSaldo;
	
	@Value("${viarapida.url.pasos}")
	private String viarapidaUrlPasos;
	
	@Value("${viarapida.code.platform}")
	private String viarapidaCodePlatform;
	
	@Value("${viarapida.pasos.tipoconsulta}")
	private int viarapidaPasosTipoconsulta;
	
}
